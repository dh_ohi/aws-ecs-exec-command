from dotenv import dotenv_values
from bitbucket_pipes_toolkit import Pipe, get_logger
from aws_ecs_task import AwsEcsTask

logger = get_logger()

variables = {
    'AWS_DEFAULT_REGION': {
        'type': 'string',
        'required': True,
    },
    'AWS_ACCESS_KEY_ID': {
        'type': 'string',
        'required': True,
    },
    'AWS_SECRET_ACCESS_KEY': {
        'type': 'string',
        'required': True,
    },
    'CLUSTER_NAME': {
        'type': 'string',
        'required': True,
    },
    'SERVICE_NAME': {
        'type': 'string',
        'default': '',
    },
    'IMAGE': {
        'type': 'string',
        'required': True,
    },
    'TASK_ROLE_ARN': {
        'type': 'string',
        'default': '',
    },
    'ENV_FILE': {
        'type': 'list',
        'default': [],
    },
    'EXTRA_PARAMETERS': {
        'type': 'dict',
        'default': {},
    },
    'INSTANCE_TYPE': {
        'type': 'list',
        'default': [],
    },
    'COMMAND': {
        'type': 'string',
        'required': True,
    },
}


class AwsEcsExecCommand(Pipe):
    def __init__(self, pipe_metadata=None, schema=None, env=None):
        # aws_env = {
        #    'AWS_DEFAULT_REGION': self.get_variable('AWS_DEFAULT_REGION'),
        #    'AWS_ACCESS_KEY_ID': self.get_variable('AWS_ACCESS_KEY_ID'),
        #    'AWS_SECRET_ACCESS_KEY': self.get_variable('AWS_SECRET_ACCESS_KEY'),
        # }
        # if env is None:
        #     env = aws_env
        # else:
        #     env.update(aws_env)
        super().__init__(pipe_metadata=pipe_metadata, schema=schema, env=env)
        print(self.variables)

    def run(self):
        super().run()
        # env_dict = dict([v.split('=') for v in self.get_variable('ENV').split(',')])
        # print(env_dict)
        # environment = [{'name': k, 'value': v} for k, v in env_dict.items()]
        # environment += [
        #     {'name': k, 'value': v}
        #     for env_file in self.get_variable('ENV_FILE')
        #     for k, v in dotenv_values(env_file).items()
        # ]
        environment = [{'name': k, 'value': v} for k, v in self.get_variable('EXTRA_PARAMETERS').items()]

        environment += [
            {'name': k, 'value': v}
            for env_file in self.get_variable('ENV_FILE')
            for k, v in dotenv_values(env_file).items()
        ]

        task = AwsEcsTask(
           cluster=self.get_variable('CLUSTER_NAME'),
           image=self.get_variable('IMAGE'),
           environment=environment,
           instance_type=self.get_variable('INSTANCE_TYPE'),
           service=self.get_variable('SERVICE_NAME'),
           task_role_arn=self.get_variable('TASK_ROLE_ARN'),
        )
        task.register_task_definition()
        task.run(self.get_variable('COMMAND'))
        exit_code, stop_reason = task.get_exit_status_code()
        # task.delete_log_stream()
        # task.deregister_task_definition()
        print(exit_code)
        if exit_code == 0:
            self.success('\033[32m[finished]\033[0m', do_exit=True)
        else:
            self.fail(f'\033[31m[failed]\033[0m exit_code: {exit_code}, reason: {stop_reason}')


if __name__ == '__main__':
    import os
    # for env in os.environ:
    #     print('======================')
    #     print(env)
    #     print(os.environ.get(env))
    #     print('======================')

    pipe = AwsEcsExecCommand(pipe_metadata='/pipe.yml', schema=variables)
    # print('++++++++++++++++++')
    # print(variables)
    pipe.log_info("Executing the pipe...")
    pipe.run()
