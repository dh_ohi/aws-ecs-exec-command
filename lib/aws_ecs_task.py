import re
import sys
import time
import boto3

from pprint import pprint


class AwsEcsTask:
    # TODO 全体的に異常処理系が甘い
    ecs = None
    logs = None
    identification_name = None
    cluster = None
    task_definition = None
    task_arn = None
    member_of = ''
    log_group = None
    log_stream = None
    service = None
    task_role_arn = None

    region_name = 'ap-northeast-1'  # TODO

    def __init__(self, **kwargs):
        self.cluster = kwargs['cluster']
        self.image = kwargs['image']
        self.environment = kwargs['environment']
        self.ecs = boto3.client(
            'ecs',
            region_name=self.region_name,
        )
        self.logs = boto3.client(
            'logs',
            region_name=self.region_name,
        )
        if self.image is not None:
            self.identification_name = re.sub(r'[^a-zA-Z0-9_-]', '_', self.image)

        self.member_of = ' or '.join(
            [f'attribute:ecs.instance-type =~ {v}.*' for v in kwargs['instance_type']]
        )
        self.service = kwargs['service']
        self.task_role_arn = kwargs['task_role_arn']

    def register_task_definition(self):
        """
        タスク定義を登録する
        """
        # TODO 途中
        if self.service is not None:
            # --serviceオプションが定義されている場合はサービス名からタスク定義取得
            response = self.ecs.describe_services(
                cluster=self.cluster,
                services=[
                    self.service,
                ],
            )
            self.task_definition = response['services'][0]['taskDefinition']
            response = self.ecs.describe_task_definition(
                taskDefinition=self.task_definition,
            )
            # self.container_definition = response['taskDefinition']['containerDefinitions'][0]
        else:
            container_name = f'aws-ecs-exec-command-{self.identification_name}'
            regex = r'_\d+$'
            bitbucket_build_number = re.search(regex, container_name).group()[1:]
            aws_log_group_name = re.sub(regex, '', container_name)
            kwargs = {
                'family': f'{container_name}-task',
                'taskRoleArn': self.task_role_arn,
                'containerDefinitions': [
                    {
                        'name': container_name,
                        'image': self.image,
                        'memoryReservation': 128,
                        'essential': True,
                        'environment': self.environment,
                        "logConfiguration": {
                            "logDriver": "awslogs",
                            "options": {
                                "awslogs-create-group": "true",
                                "awslogs-region": self.region_name,
                                "awslogs-group": aws_log_group_name,
                                "awslogs-stream-prefix": bitbucket_build_number,
                                # "log_retention_days": "30", TODO サポートされてない？
                            },
                        },
                    },
                ],
            }

            if len(self.member_of) > 0:
                kwargs['placementConstraints'] = [
                    {
                        'type': 'memberOf',
                        'expression': self.member_of,
                    }
                ]
            response = self.ecs.register_task_definition(**kwargs)

        self.task_definition = response['taskDefinition']['taskDefinitionArn']
        self.container_definition = response['taskDefinition']['containerDefinitions'][0]
        self.log_configuration = self.container_definition['logConfiguration']

    def get_log_message(self, start_time=None):
        """
        タスク実行のログを返す
        """
        error_message = None
        kwargs = {
            'logGroupName': self.log_group,
            'logStreamName': self.log_stream,
        }
        if start_time is not None:
            # start_timeの引数が未定義の場合はログ全体のメッセージを返す
            kwargs['startTime'] = start_time + 1
        for _ in range(10):
            try:
                response = self.logs.get_log_events(**kwargs)
            except Exception as e:
                # TODO 取得するExceptionを明確にしたい
                # eventsが取得できなかったら5秒wait
                error_message = e
                time.sleep(5)
            else:
                break
        else:
            # 10回試行して失敗したらエラー
            print(
                'get log event failed(log_group: {}, log_stream: {})【{}】'.format(
                    self.log_group,
                    self.log_stream,
                    error_message,
                )
            )
            return '', start_time

        if len(events := response['events']) > 0:
            return '\n'.join([event['message'] for event in events]), events[-1]['timestamp']
        else:
            return '', start_time

    def run(self, command):
        """
        タスクを実行する
        """
        command = ['python3', 'manage.py']

        print('\033[33m[exec command]\033[0m ' + ' '.join(command) + '\n')

        response = self.ecs.run_task(
            cluster=self.cluster,
            taskDefinition=self.task_definition,
            count=1,
            overrides={
                'containerOverrides': [
                    {
                        'name': self.container_definition['name'],
                        # 'command': ['sh', '-c'] + command,
                        'command': command,
                    },
                ],
            },
        )

        if len(response['tasks']) == 0:
            # response['tasks'] が空リストならエラー
            raise Exception(response['failures'][0]['reason'])

        self.task_arn = response['tasks'][0]['taskArn']
        task_id = re.search(r'([^\/]+?)?$', self.task_arn).group()

        self.log_group = self.log_configuration['options']['awslogs-group']
        self.log_stream = '{}/{}/{}'.format(
            self.log_configuration["options"]["awslogs-stream-prefix"],
            self.container_definition["name"],
            task_id,
        )

        sleep_count = 0
        wait = 1
        start_time = 0
        while not self.is_finished():
            # if self.to_exit:
            #     break
            if wait < 60 and sleep_count % 10 == 0:
                wait *= 2
            if wait > 60:
                wait = 60
            message, start_time = self.get_log_message(start_time)
            if message is not False and len(message) > 0:
                print(message)
            time.sleep(wait)
            sleep_count += 1

        message, _ = self.get_log_message(start_time)

    def delete_log_stream(self):
        """
        タスク実行のログストリームを削除する
        """
        self.logs.delete_log_stream(
            logGroupName=self.log_group,
            logStreamName=self.log_stream,
        )

    def get_exit_status_code(self):
        """
        TODO
        終了ステータスコードを取得する
        def exit_code(self):
            if self.task_arn is None:
                print('task_arn is None', file=sys.stderr)
                return None
            result = self.ecs.describe_tasks(cluster=self.cluster,
                                             tasks=[self.task_arn])
            if len(result['tasks']) == 0:
                print('cannot find task', self.task_arn, file=sys.stderr)
                return None
            task = result['tasks'][0]
            if task['lastStatus'] != 'STOPPED':
                return None
            if 'stoppedReason' in task:
                if task['stoppedReason'] != 'Essential container in task exited':
                    print(task['stoppedReason'], file=sys.stderr)
            for c in task['containers']:
                status = {'exitCode': 255}
                if 'exitCode' in c:
                    status['exitCode'] = c['exitCode']
                if 'reason' in c:
                    status['reason'] = re.sub(r'\n+$', '', c['reason'])
                if len(result['failures']) > 0:
                    status['failures'] = result['failures']
                return status
            return None

            'stopCode': 'TaskFailedToStart'|'EssentialContainerExited'|'UserInitiated',
        """

        if self.task_arn is None:
            return 255, 'task_arn is None'

        response = self.ecs.describe_tasks(
            cluster=self.cluster,
            tasks=[
                self.task_arn,
            ],
        )

        if len(response['tasks']) == 0:
            return 255, f'cannot find task {self.task_arn}'

        task = response['tasks'][0]

        if task['lastStatus'] != 'STOPPED':
            return 255, f'last status is not STOPPED: {task["lastStatus"]}'

        exit_code = 0
        stop_reason = ''

        container = task['containers'][0]
        if 'stopCode' in task \
                and task['stopCode'] != 'EssentialContainerExited':
            if 'reason' in container:
                stop_reason = container['reason']
            else:
                stop_reason = task['stoppedReason']

            exit_code = 255

        if 'exitCode' in container:
            exit_code = container['exitCode']

        return exit_code, stop_reason

    def deregister_task_definition(self):
        """
        タスク定義を削除する
        """
        self.ecs.deregister_task_definition(
            taskDefinition=self.task_definition,
        )

    def is_finished(self):
        """
        タスクが終了していたらTrueを返す
        """
        if self.task_arn is None:
            print('task_arn is None', file=sys.stderr)
            return True

        response = self.ecs.describe_tasks(
            cluster=self.cluster,
            tasks=[
                self.task_arn,
            ],
        )

        if len(response['tasks']) == 0:
            print('missing task:', self.task_arn, file=sys.stderr)
            return False

        for task in response['tasks']:
            if task['lastStatus'] != 'STOPPED':
                return False

        return True
