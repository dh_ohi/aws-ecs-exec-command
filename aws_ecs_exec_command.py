import sys
import argparse
from dotenv import dotenv_values
from lib import AwsEcsTask


def main(args):
    environment = [{'name': k, 'value': v} for k, v in args.env_list]
    environment += [
        {'name': k, 'value': v}
        for env_file in args.env_file_list
        for k, v in dotenv_values(env_file).items()
    ]

    task = AwsEcsTask(
        cluster=args.cluster,
        image=args.image,
        environment=environment,
        instance_type=args.instance_type,
        service=args.service,
        task_role_arn=args.task_role_arn,
    )
    task.register_task_definition()
    task.run(args.command)
    exit_code, stop_reason = task.get_exit_status_code()
    if exit_code == 0:
        print('\033[32m[finished]\033[0m')
    else:
        print(f'\033[31m[failed]\033[0m exit_code: {exit_code}, reason: {stop_reason}', file=sys.stderr)
    # task.delete_log_stream()
    task.deregister_task_definition()
    exit(exit_code)


if __name__ == "__main__":
    """
    サービスから呼び出す場合必須なオプション
    cluster, service

    コンテナイメージから呼び出す場合必須なオプション
    cluster, image, task_role_arn
    """
    parser = argparse.ArgumentParser(description='')
    parser.add_argument(
        '-c',
        '--cluster',
        required=True,
    )
    # TODO サービスからタスク定義を読み出せるようにする
    parser.add_argument(
        '-s',
        '--service',
    )
    parser.add_argument(
        '-r',
        '--region_name',
        default='ap-northeast-1',
    )
    parser.add_argument(
        '-i',
        '--image',
    )
    parser.add_argument(
        '-e',
        '--env',
        action='append',
        type=lambda kv: kv.split('='),  # TODO key=value出なかった場合の考慮
        default=[],
        dest='env_list'
    )
    parser.add_argument(
        '-t',
        '--task_role_arn',
    )
    parser.add_argument(
        '--env_file',
        action='append',
        default=[],
        dest='env_file_list',
    )
    parser.add_argument(
        '--instance_type',
        nargs='*',
        default=['c'],
    )
    parser.add_argument(
        'command',
        type=str,
    )

    args = parser.parse_args()
    main(args)
