FROM python:3.9-slim

WORKDIR /opt/aws_ecs_exec_command/

COPY requirements*.txt ./
RUN pip install -r ./requirements-pipe.txt

COPY LICENSE.txt pipe.yml README.md ./
COPY pipe lib ./

CMD ["python3", "/opt/aws_ecs_exec_command/pipe.py"]
